## skl

The *standard kozet library*.

Currently header-only, so just put `include/` on your include path and you're good to go!

### Components

* `skl/detail.h`: utilities for implementing the tricker stuff from other parts of the library; maybe you'll find them useful too.
* `skl/TupleVector.h`: a container that stores elements SoA-ly.
* `skl/GenerationalAllocator.h`: manages (generation, index) pairs so that each handle allocated is unique, and no two handles with the same index are live at the same time

### TupleVector

#### Features that are in `std::vector` but currently not `TupleVector`

* Passing in an allocator, or getting the allocator, as currently we have an allocator on each underlying vector, so unless there's somehow a typeless allocator, I don't see this being here
* Constructors that:
  * initialise the vector with repeating values
  * take in iterators
  * take in `initializer_list`
* `data()` doesn't make sense for this container
* `->` cannot be used with iterators, as they point to a proxy type
* no `max_size()` yet
* `insert()`, `emplace()`, `erase()`, `swap()` NYI
* comparison operators NYI

Also need to test performance. Initial prospects look good, as long as you:

* Compile with `-O3`
* Disable assertions

#### Features that are in `TupleVector` but not `std::vector`

* get the underlying `std::vector`s or pointers to their data
* `emplace_back_piecewise`: construct an element at the back, element by element
