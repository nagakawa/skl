/*
   Copyright 2018 AGC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <iostream>
#include <random>
#include <string>
#include <vector>

#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch.hpp>
#include <skl/DoubleLookupMap.h>
#include <skl/GenerationalAllocator.h>
#include <skl/SlotMap.h>
#include <skl/TupleVector.h>
#include <skl/test_types.h>

TEST_CASE("Try to declare a tuple vector", "[basic]") {
  skl::TupleVector<std::vector, std::tuple<int, bool>> aaa;
  REQUIRE(aaa.get<0>().empty());
  REQUIRE(aaa.get<1>().empty());
  REQUIRE(2 + 2 == 4);
}

TEST_CASE("Test indexing", "[basic]") {
  // Note: this is just for show. Don't actually use `double` to store currency.
  skl::TupleVector<std::vector, std::tuple<std::string, int, double>> products;
  products.push_back(std::tuple("Useless Apple", 5, 0.82));
  products.push_back(std::tuple("Dead Hamster", 6, 5.31));
  products.push_back(std::tuple("Egg", 12, 0.66));
  REQUIRE(products[1].get<int>() == 6);
  REQUIRE(products[1].get<0>() == "Dead Hamster");
  auto first = products.front();
  REQUIRE(first.get<2>() == Approx(0.82));
  std::tuple<std::string, int, double> last = products.back();
  REQUIRE(std::get<int>(last) == 12);
  REQUIRE(products[2].get<std::string>() == "Egg");
}

TEST_CASE("Test iteration", "[basic]") {
  // Note: this is just for show. Don't actually use `double` to store currency.
  skl::TupleVector<std::vector, std::tuple<std::string, int, double>> products;
  products.push_back(std::tuple("Useless Apple", 5, 0.82));
  products.push_back(std::tuple("Dead Hamster", 6, 5.31));
  products.push_back(std::tuple("Egg", 12, 0.66));
  const auto& const_products = products;
  double total_price = 0;
  for (const auto& entry : const_products) {
    total_price += entry.get<int>() * entry.get<double>();
  }
  REQUIRE(total_price == 43.88);
  for (auto entry : products) { entry.get<int>() -= 1; }
  int total_items = 0;
  for (const auto& entry : const_products) { total_items += entry.get<int>(); }
  REQUIRE(total_items == 20);
}

TEST_CASE("Test reverse iteration", "[basic]") {
  // Note: this is just for show. Don't actually use `double` to store currency.
  skl::TupleVector<std::vector, std::tuple<std::string, int, double>> products;
  products.push_back(std::tuple("Useless Apple", 5, 0.82));
  products.push_back(std::tuple("Dead Hamster", 6, 5.31));
  products.push_back(std::tuple("Egg", 12, 0.66));
  const auto& const_products = products;
  double total_price = 0;
  std::for_each(
      const_products.rbegin(), const_products.rend(), [&](auto entry) {
        total_price += entry.template get<int>() * entry.template get<double>();
      });
  REQUIRE(total_price == 43.88);
  std::for_each(products.rbegin(), products.rend(), [&](auto entry) {
    entry.template get<int>() -= 1;
  });
  int total_items = 0;
  std::for_each(
      const_products.rbegin(), const_products.rend(),
      [&](auto entry) { total_items += entry.template get<int>(); });
  REQUIRE(total_items == 20);
}

TEST_CASE("Test emplacement", "[basic]") {
  // Note: this is just for show. Don't actually use `double` to store currency.
  skl::TupleVector<std::vector, std::tuple<std::string, int, double>> products;
  products.push_back(std::tuple("Useless Apple", 5, 0.82));
  products.emplace_back_piecewise(
      std::tuple(6, 'A'), std::tuple(7), std::tuple(5.42));
  products.emplace_back("Egg", 12, 0.66);
  REQUIRE(products[1].get<std::string>() == "AAAAAA");
  REQUIRE(products[1].get<int>() == 7);
  REQUIRE(products[1].get<double>() == 5.42);
  REQUIRE(products[2].get<std::string>() == "Egg");
  REQUIRE(products[2].get<int>() == 12);
  REQUIRE(products[2].get<double>() == 0.66);
}

TEST_CASE("Test pop_back", "[basic]") {
  // Note: this is just for show. Don't actually use `double` to store currency.
  skl::TupleVector<std::vector, std::tuple<std::string, int, double>> products;
  products.push_back(std::tuple("Useless Apple", 5, 0.82));
  products.push_back(std::tuple("Dead Hamster", 6, 5.31));
  products.push_back(std::tuple("Egg", 12, 0.66));
  products.pop_back();
  REQUIRE(products.size() == 2);
  REQUIRE(products.back().get<0>() == "Dead Hamster");
}

TEST_CASE(
    "Make sure emplace_back_piecewise and set_element_piecewise don't copy",
    "[basic]") {
  skl::TupleVector<std::vector, std::tuple<skl::TrackCopy<std::string>, int>>
      widgets;
  widgets.emplace_back_piecewise(std::tuple("Hello"), std::tuple(5));
  widgets.emplace_back_piecewise(
      std::tuple("Goodbye cruel world"), std::tuple(5));
  widgets.set_element_piecewise(1, std::tuple("Widget B"), std::tuple(7));
  REQUIRE(widgets[0].get<0>().item() == "Hello");
  REQUIRE(widgets[0].get<0>().copy_count() == 0);
  REQUIRE(widgets[1].get<0>().item() == "Widget B");
  REQUIRE(widgets[1].get<0>().copy_count() == 0);
  widgets[1].set_piecewise(std::tuple(6, 'A'), std::tuple(69));
  REQUIRE(widgets[1].get<0>().item() == "AAAAAA");
  REQUIRE(widgets[1].get<0>().copy_count() == 0);
}

TEST_CASE("Test generational allocator", "[basic]") {
  skl::GenerationalAllocator<> ga;
  bool junk;
  auto first_handle = ga.allocate(junk);
  REQUIRE(ga.is_live(first_handle));
  REQUIRE(ga.is_live(first_handle.index));
  REQUIRE(ga.get_generation(first_handle.index) == first_handle.generation);
  auto second_handle = ga.allocate(junk);
  REQUIRE(second_handle != first_handle);
  ga.release(first_handle);
  auto third_handle = ga.allocate(junk);
  REQUIRE(!ga.is_live(first_handle));
  REQUIRE(first_handle != third_handle);
  REQUIRE(second_handle != third_handle);
  ga.release(second_handle);
  std::array<decltype(ga)::Handle, 299> moar_handles;
  size_t existing;
  ga.allocm(moar_handles.size(), moar_handles.data(), existing);
  for (auto h : moar_handles) { REQUIRE(ga.is_live(h)); }
  REQUIRE(ga.count_extant() == 300);
}

TEST_CASE("Test slot map of std::vector", "[basic]") {
  skl::SlotMap<std::vector<int>> my_numbers;
  REQUIRE(my_numbers.size() == 0);
  auto h1 = my_numbers.insert(5835); // A special number to kozet's heart
  REQUIRE(my_numbers.size() == 1);
  REQUIRE(*h1 == 5835);
  my_numbers.insert(44); // haha arka sex number
  for (int i = 3; i <= 7; ++i) my_numbers.insert(i);
  my_numbers.erase(h1);
  REQUIRE(my_numbers.size() == 6);
  int sum = 0;
  for (int n : my_numbers) sum += n;
  REQUIRE(sum == 69); // haha english sex number
  sum = 0;
  std::for_each(
      my_numbers.rbegin(), my_numbers.rend(), [&](int n) { sum += n; });
  REQUIRE(sum == 69);
}

TEST_CASE("Test indexing and find of slot map", "[basic]") {
  skl::SlotMap<std::vector<int>> my_numbers;
  my_numbers.insert(55);
  auto h1 = my_numbers.insert(5835);
  my_numbers.insert(23);
  auto k1 = h1.key();
  auto h2 = my_numbers.insert(8);
  auto k2 = h2.key();
  my_numbers.erase(k2);
  my_numbers.insert(-1);
  REQUIRE(my_numbers[k1] == 5835);
  REQUIRE(my_numbers.at(k1) == 5835);
  REQUIRE(my_numbers.find(k1) == h1);
  REQUIRE(my_numbers.find(k2) == my_numbers.end());
}

TEST_CASE("Test slot map of TupleVector", "[basic]") {
  skl::SlotMap<
      skl::TupleVector<std::vector, std::tuple<std::string, int, double>>>
      products;
  REQUIRE(products.size() == 0);
  auto h1 = products.emplace("Useless Apple", 5, 0.82);
  REQUIRE(products.size() == 1);
  REQUIRE((*h1).get<0>() == "Useless Apple");
  products.insert(std::tuple("Dead Hamster", 6, 5.31));
  REQUIRE(products.size() == 2);
  products.erase(h1);
  REQUIRE(products.size() == 1);
  products.insert(std::tuple("Egg", 12, 0.66));
  const auto& const_products = products;
  double total_price = 0;
  for (const auto& entry : const_products) {
    total_price += entry.get<int>() * entry.get<double>();
  }
  REQUIRE(total_price == 39.78);
}

TEST_CASE("Test DoubleLookupMap", "[basic]") {
  using SM = skl::SlotMap<std::vector<std::string>>;
  skl::DoubleLookupMap<
      std::string, typename SM::key_type, std::string,
      std::unordered_map<std::string, typename SM::key_type>, SM>
      conlangs;
  REQUIRE(conlangs.size() == 0);
  auto h = conlangs.insert("Ŋarâþ Crîþ", "kozet");
  REQUIRE(h.has_value());
  REQUIRE(conlangs.size() == 1);
  auto h2 = conlangs.insert("Drsk", "Isoraķatheð");
  REQUIRE(h2.has_value());
  REQUIRE(conlangs.size() == 2);
  auto h3 = conlangs.insert("Ŋarâþ Crîþ", "ostracod"); // Lie!
  REQUIRE(!h3.has_value());
  REQUIRE(conlangs.size() == 2);
  auto should_be_iso = conlangs.lookup("Drsk");
  REQUIRE(should_be_iso != nullptr);
  REQUIRE(*should_be_iso == "Isoraķatheð");
  auto should_be_kozet = conlangs.lookup("Ŋarâþ Crîþ");
  REQUIRE(should_be_kozet != nullptr);
  REQUIRE(*should_be_kozet == "kozet");
  auto should_be_none = conlangs.lookup("Vötgil");
  REQUIRE(should_be_none == nullptr);
  bool erased1 = conlangs.erase("Vötgil");
  REQUIRE(!erased1);
  bool erased2 = conlangs.erase("Ŋarâþ Crîþ");
  REQUIRE(erased2);
  REQUIRE(conlangs.size() == 1);
  auto should_also_be_none = conlangs.lookup("Ŋarâþ Crîþ");
  REQUIRE(should_also_be_none == nullptr);
}
