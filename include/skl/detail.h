#pragma once
#ifndef SKL_DETAIL_H
#  define SKL_DETAIL_H

#  include <tuple>
#  include <type_traits>
#  include <utility>

namespace skl {
  /**
   * \brief Always false.
   *
   * This is useful for `static_assert`s.
   */
  template<typename T> constexpr bool fail = false;

  namespace detail {
    template<typename T, typename U> struct TupleCons_;
    template<typename T, typename... Args>
    struct TupleCons_<T, std::tuple<Args...>> {
      using Type = std::tuple<T, Args...>;
    };
  }

  /**
   * \brief Prepends a type to a tuple type.
   *
   * In other words, `TupleCons<T, std::tuple<U...>>` becomes
   * `std::tuple<T, U...>`.
   */
  template<typename T, typename U>
  using TupleCons = typename detail::TupleCons_<T, U>::Type;

  /**
   * \brief Expands to a tuple with a type function applied to each element of
   * a parameter pack.
   *
   * For example, `MapParameterPack<std::vector, A, B, C>` becomes
   * `std::tuple<std::vector<A>, std::vector<B>, std::vector<C>>`.
   */
  template<template<typename> typename F, typename... Args>
  using MapParameterPack = std::tuple<F<Args>...>;

  namespace detail {
    template<typename F, typename... Args, size_t... seq>
    void tuple_for_each_(
        F&& f, std::tuple<Args...>& t, std::index_sequence<seq...>) {
      [[maybe_unused]] auto l = {(f(std::get<seq>(t)), 0)...};
    }
    template<typename F, typename... Args, size_t... seq>
    void tuple_for_each_(
        F&& f, const std::tuple<Args...>& t, std::index_sequence<seq...>) {
      [[maybe_unused]] auto l = {(f(std::get<seq>(t)), 0)...};
    }
    template<typename F, typename... Args, size_t... seq>
    void tuple_for_each_index_(
        F&& f, std::tuple<Args...>& t, std::index_sequence<seq...>) {
      [[maybe_unused]] auto l = {
          (f(std::get<seq>(t), std::integral_constant<size_t, seq>{}), 0)...};
    }
    template<typename F, typename... Args, size_t... seq>
    void tuple_for_each_index_(
        F&& f, const std::tuple<Args...>& t, std::index_sequence<seq...>) {
      [[maybe_unused]] auto l = {
          (f(std::get<seq>(t), std::integral_constant<size_t, seq>{}), 0)...};
    }
  }

  template<typename F, typename... Args>
  void tuple_for_each(F&& f, std::tuple<Args...>& t) {
    detail::tuple_for_each_(
        std::forward<F>(f), t, std::index_sequence_for<Args...>());
  }

  template<typename F, typename... Args>
  void tuple_for_each(F&& f, const std::tuple<Args...>& t) {
    detail::tuple_for_each_(
        std::forward<F>(f), t, std::index_sequence_for<Args...>());
  }

  template<typename F, typename... Args>
  void tuple_for_each_index(F&& f, std::tuple<Args...>& t) {
    detail::tuple_for_each_index_(
        std::forward<F>(f), t, std::index_sequence_for<Args...>());
  }

  template<typename F, typename... Args>
  void tuple_for_each_index(F&& f, const std::tuple<Args...>& t) {
    detail::tuple_for_each_index_(
        std::forward<F>(f), t, std::index_sequence_for<Args...>());
  }

  namespace detail {
    template<size_t n, typename... Args> struct NthType_;
    template<size_t n> struct NthType_<n> {
      static_assert(fail<n>, "Index out of bounds");
    };
    template<typename Arg1, typename... Args>
    struct NthType_<0, Arg1, Args...> {
      using Type = Arg1;
    };
    template<size_t n, typename Arg1, typename... Args>
    struct NthType_<n, Arg1, Args...> {
      using Type = typename NthType_<n - 1, Args...>::Type;
    };

    template<typename I, size_t n, I... args> struct NthValueImpl_;
    template<typename I, size_t n> struct NthValueImpl_<I, n> {
      static_assert(fail<n>, "Index out of bounds");
    };
    template<typename I, I arg1, I... args>
    struct NthValueImpl_<I, 0, arg1, args...> {
      static constexpr I value = arg1;
    };
    template<typename I, size_t n, I arg1, I... args>
    struct NthValueImpl_<I, n, arg1, args...> {
      static constexpr I value = NthValueImpl_<I, n - 1, args...>::value;
    };
    template<typename I, size_t n, typename Seq> struct NthValue_;
    template<typename I, size_t n, I... Args>
    struct NthValue_<I, n, std::index_sequence<Args...>> {
      static constexpr I value = NthValueImpl_<I, n, Args...>::value;
    };
  }

  /**
   * \brief Index an element of a parameter pack.
   *
   * For example, `NthType<1, A, B, C>` becomes `B`.
   *
   * An error will be thrown when the index is out of bounds.
   */
  template<size_t n, typename... Args>
  using NthType = typename detail::NthType_<n, Args...>::Type;
  /**
   * \brief Index an element of an index sequence.
   *
   * For example, `nth_value<size_t, 1, std::index_sequence<5, 3, 2>>`
   * becomes `3`.
   *
   * An error will be thrown when the index is out of bounds.
   */
  template<typename I, size_t n, typename Seq>
  constexpr I nth_value = detail::NthValue_<I, n, Seq>::value;

  namespace detail {
    template<typename I, I i, typename Seq> struct IntSequenceCons_;
    template<typename I, I i, I... j>
    struct IntSequenceCons_<I, i, std::integer_sequence<I, j...>> {
      using Type = std::integer_sequence<I, i, j...>;
    };
  }

  /**
   * \brief Prepend an integer to an integer sequence.
   *
   * For example, `IntSequenceCons<size_t, 5, std::index_sequence<4, 2>>`
   * expands to `std::index_sequence<5, 4, 2>`.
   */
  template<typename I, I i, typename Seq>
  using IntSequenceCons = typename detail::IntSequenceCons_<I, i, Seq>::Type;

  namespace detail {
    template<typename I, I i, typename Seq> struct AddToEach_;
    template<typename I, I i, I... j>
    struct AddToEach_<I, i, std::integer_sequence<I, j...>> {
      using Type = std::integer_sequence<I, (i + j)...>;
    };
  }

  /**
   * \brief Add an integer to each element an integer sequence.
   *
   * For example, `AddToEach<size_t, 5, std::index_sequence<4, 2>>`
   * expands to `std::index_sequence<9, 7>`.
   */
  template<typename I, I i, typename Seq>
  using AddToEach = typename detail::AddToEach_<I, i, Seq>::Type;

  namespace detail {
    template<typename T, typename... Args> struct Where_;
    template<typename T> struct Where_<T> {
      using Type = std::index_sequence<>;
    };
    template<typename T, typename Arg1, typename... Args>
    struct Where_<T, Arg1, Args...> {
      using Tail = AddToEach<size_t, 1, typename Where_<T, Args...>::Type>;
      using Type = std::conditional_t<
          std::is_same_v<T, Arg1>, IntSequenceCons<size_t, 0, Tail>, Tail>;
    };
  }

  /**
   * \brief Return a sequence of indices where `T` matches elements of `Args`.
   */
  template<typename T, typename... Args>
  using Where = typename detail::Where_<T, Args...>::Type;

  namespace detail {
    template<typename T, typename... Args> struct FindType_ {
      using Indices = Where<T, Args...>;
      static constexpr size_t value =
          Indices::size() == 1 ? nth_value<size_t, 0, Indices> : -1;
    };
  }

  /**
   * \brief If there is exactly one element of `Args...` that matches `T`,
   * then return its index. Otherwise, return `-1`.
   */
  template<typename T, typename... Args>
  constexpr size_t find_type = detail::FindType_<T, Args...>::value;

  namespace detail {
    template<template<typename...> typename S, typename T>
    struct Instantiates_ : std::false_type {};
    template<template<typename...> typename S, typename... Args>
    struct Instantiates_<S, S<Args...>> : std::true_type {};
  }

  template<template<typename...> typename S, typename T>
  constexpr bool instantiates = detail::Instantiates_<S, T>::value;

  // Tests
  namespace detail {
    // This is here just to test that MapParameterPack works.
    template<typename T> using Ptr = T*;
  }

  static_assert(
      std::is_same_v<
          MapParameterPack<detail::Ptr, int, double>,
          std::tuple<int*, double*>>,
      "MapParameterPack doesn't work properly");
  static_assert(std::is_same_v<
                AddToEach<size_t, 5, std::index_sequence<4, 2>>,
                std::index_sequence<9, 7>>);
  static_assert(std::is_same_v<
                Where<int, int, double, bool, int, int, void*>,
                std::index_sequence<0, 3, 4>>);
}

#endif // SKL_DETAIL_H
