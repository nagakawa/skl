#pragma once
#ifndef SKL_DOUBLE_LOOKUP_MAP
#  define SKL_DOUBLE_LOOKUP_MAP

#  include <type_traits>

namespace skl {
  template<typename K, typename H, typename V, typename M1, typename M2>
  class DoubleLookupMap {
    static_assert(
        std::is_same_v<typename M1::key_type, K>, "M1 maps from K to H");
    static_assert(
        std::is_same_v<typename M1::mapped_type, H>, "M1 maps from K to H");
    static_assert(
        std::is_same_v<typename M2::key_type, H>, "M2 maps from H to V");
    static_assert(
        std::is_same_v<typename M2::mapped_type, V>, "M2 maps from H to V");
    static_assert(M2::is_slot_map, "M2 should be a SlotMap-like type");

  public:
    using primary_key_type = K;
    using secondary_key_type = H;
    using mapped_type = V;
    using primary_map_type = M1;
    using secondary_map_type = M2;
    using size_type = size_t;
    using difference_type = ptrdiff_t;

    DoubleLookupMap() = default;
    DoubleLookupMap(const DoubleLookupMap& other) = default;
    DoubleLookupMap(DoubleLookupMap&& other) = default;
    DoubleLookupMap& operator=(const DoubleLookupMap& other) = default;
    DoubleLookupMap& operator=(DoubleLookupMap&& other) = default;

    template<typename K2, typename V2>
    std::optional<secondary_key_type> insert(K2&& key, V2&& value) {
      if (primary.find(key) != primary.end()) return std::nullopt;
      secondary_key_type handle =
          secondary.insert(std::forward<V2>(value)).key();
      primary.emplace(std::forward<K2>(key), handle);
      return handle;
    }

    template<typename... Ks, typename... Vs>
    std::optional<secondary_key_type>
    emplace(std::tuple<Ks...>&& key, std::tuple<Vs...>&& value) {
      secondary_key_type handle =
          std::apply(
              [this](auto&&... args) {
                return secondary.emplace(std::forward<decltype(args)>(args)...);
              },
              std::forward<decltype(value)>(value))
              .key();
      auto [it, stat] = primary.emplace(
          std::piecewise_construct, std::forward<decltype(key)>(key),
          std::tuple{handle});
      (void) it;
      if (!stat) {
        secondary.erase(handle);
        return std::nullopt;
      }
      return handle;
    }

    size_t size() const { return primary.size(); }

    template<typename K2> bool erase(const K2& key) {
      auto it = primary.find(key);
      if (it == primary.end()) return false;
      secondary.erase(it->second);
      primary.erase(it);
      return true;
    }

    template<typename K2>
    std::optional<secondary_key_type> lookup_primary(const K2& key) const {
      auto it = primary.find(key);
      if (it == primary.end()) return std::nullopt;
      return it->second;
    }

    const mapped_type* lookup_secondary(const secondary_key_type& key) const {
      auto it = secondary.find(key);
      if (it == secondary.end()) return nullptr;
      return &*it;
    }

    mapped_type* lookup_secondary(const secondary_key_type& key) {
      auto it = secondary.find(key);
      if (it == secondary.end()) return nullptr;
      return &*it;
    }

    template<typename K2> const mapped_type* lookup(const K2& key) const {
      auto h = lookup_primary(key);
      if (!h.has_value()) return nullptr;
      return lookup_secondary(*h);
    }

    template<typename K2> mapped_type* lookup(const K2& key) {
      auto h = lookup_primary(key);
      if (!h.has_value()) return nullptr;
      return lookup_secondary(*h);
    }

  private:
    M1 primary;
    M2 secondary;
  };
}

#endif // SKL_DOUBLE_LOOKUP_MAP
