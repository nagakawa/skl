#pragma once
#ifndef SKL_TUPLEVECTOR_H
#  define SKL_TUPLEVECTOR_H

#  include <assert.h>

#  include <iterator>
#  include <memory>

#  include "skl/detail.h"

namespace skl {
  /**
   * \brief A dynamic array type that lays out elements in a struct-or-arrays
   *   fashion.
   *
   * \tparam Vec A container type such as `std::vector`.
   * \tparam T A tuple of one or more types.
   * \tparam Allocator A type function that takes a type and returns an
   * allocator of that type.
   */
  template<
      template<typename /*T*/, typename /*Allocator*/> typename Vec, typename T,
      template<typename /*T*/> typename Allocator = std::allocator>
  class TupleVector {
    static_assert(fail<T>, "I only take tuple types");
  };

  template<
      template<typename /*T*/, typename /*Allocator*/> typename Vec,
      template<typename /*T*/> typename Allocator, typename... Args>
  class TupleVector<Vec, std::tuple<Args...>, Allocator> {
  private:
    // This has to be at the top so public methods can use it
    template<typename T> using V = Vec<T, Allocator<T>>;

  public:
    static_assert(
        sizeof...(Args) > 0, "Can't create a TupleVector of an empty tuple!");
    using Self = TupleVector<Vec, std::tuple<Args...>, Allocator>;
    using value_type = std::tuple<Args...>;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    template<bool is_const> class Iterator;

    TupleVector() = default;
    TupleVector(const Self& other) = default;
    TupleVector(Self&& other) noexcept = default;

    /**
     * \brief Construct a \ref TupleVector with \ref count elements.
     */
    explicit TupleVector(size_t count) { resize(count); }

    // Low-level accessors
    /// Get the number of elements in this container.
    size_t size() const noexcept { return std::get<0>(underlying).size(); }
    /// Get the number of elements that can be stored in this container before
    /// resizing.
    size_t capacity() const noexcept {
      return std::get<0>(underlying).capacity();
    }
    /// Return true if there are no elements in this container.
    bool empty() const noexcept { return size() == 0; }
    /**
     * \brief Get the `i`th underlying vector.
     */
    template<size_t i> const V<NthType<i, Args...>>& get() const noexcept {
      return std::get<i>(underlying);
    }
    /**
     * \brief Get the `i`th underlying vector.
     *
     * Note that you must not change the size of the vector returned from
     * this method, or havoc might ensue!
     */
    template<size_t i> V<NthType<i, Args...>>& get() noexcept {
      return std::get<i>(underlying);
    }
    /**
     * \brief Get a pointer to the start of the `i`th underlying vector's data.
     */
    template<size_t i> const NthType<i, Args...>* data() const noexcept {
      return std::get<i>(underlying).data();
    }
    /**
     * \brief Get a pointer to the start of the `i`th underlying vector's data.
     */
    template<size_t i> NthType<i, Args...>* data() noexcept {
      return std::get<i>(underlying).data();
    }
    /**
     * \brief Get the underlying vector for type `T`, if there is exactly
     * one such vector.
     */
    template<typename T> const V<T>& get() const noexcept {
      return std::get<find_type<T, Args...>>(underlying);
    }
    /**
     * \brief Get the underlying vector for type `T`, if there is exactly
     * one such vector.
     *
     * Note that you must not change the size of the vector returned from
     * this method, or havoc might ensue!
     */
    template<typename T> V<T>& get() noexcept {
      return std::get<find_type<T, Args...>>(underlying);
    }
    /**
     * \brief Get the pointer to the start of the data for the underlying
     * vector of type `T`, if there is exactly one such vector.
     */
    template<typename T> const T* data() const noexcept {
      return std::get<find_type<T, Args...>>(underlying).data();
    }
    /**
     * \brief Get the pointer to the start of the data for the underlying
     * vector of type `T`, if there is exactly one such vector.
     */
    template<typename T> T* data() noexcept {
      return std::get<find_type<T, Args...>>(underlying).data();
    }
    /**
     * \brief Resize the container.
     */
    void resize(size_t count) {
      tuple_for_each([count](auto& v) { v.resize(count); }, underlying);
    }
    /**
     * \brief Reserve space for elements.
     */
    void reserve(size_t count) {
      tuple_for_each([count](auto& v) { v.reserve(count); }, underlying);
    }
    /**
     * \brief Free unused memory in the underlying containers.
     */
    void shrink_to_fit() {
      tuple_for_each([](auto& v) { v.shrink_to_fit(); }, underlying);
    }
    /**
     * \brief Clear the container.
     */
    void clear() {
      tuple_for_each([](auto& v) { v.clear(); }, underlying);
    }

    // Methods that require proxy types
    // This type is named `reference` (lowercase) to match the C++ standard
    // library's naming.
    /**
     * \brief A mutable 'reference' to an element of the container.
     *
     * Because of how elements are laid out in this container, this is not a
     * real reference, but rather a proxy type.
     *
     * Note that `std::get` does not work on this type. The `.get` method can be
     * used instead.
     */
    class Reference {
    public:
      Reference& operator=(value_type value) {
        tuple_for_each_index(
            [&value, this](auto& elem, auto i) {
              elem[index] = std::move(std::get<i.value>(value));
            },
            container->underlying);
        return *this;
      }
      operator value_type() const& {
        return convert_impl(std::index_sequence_for<Args...>{});
      }
      operator value_type() & {
        return convert_impl(std::index_sequence_for<Args...>{});
      }
      operator value_type() && {
        return convert_impl(std::index_sequence_for<Args...>{});
      }
      template<size_t i> const NthType<i, Args...>& get() const noexcept {
        return std::get<i>(container->underlying)[index];
      }
      template<size_t i> NthType<i, Args...>& get() noexcept {
        return std::get<i>(container->underlying)[index];
      }
      template<typename T> const T& get() const noexcept {
        return std::get<find_type<T, Args...>>(container->underlying)[index];
      }
      template<typename T> T& get() noexcept {
        return std::get<find_type<T, Args...>>(container->underlying)[index];
      }
      /// Set this element by constructing each component of a tuple.
      template<typename... A> void set_piecewise(A&&... args) {
        auto value = std::tuple(std::forward<A>(args)...);
        tuple_for_each_index(
            [&value, this](auto& elem, auto j) {
              std::apply(
                  [&elem, this](auto&&... args) {
                    // Yeah, this monstrosity again.
                    using E = typename std::decay_t<decltype(elem)>::value_type;
                    elem[index] = E(std::forward<decltype(args)>(args)...);
                  },
                  std::get<j.value>(value));
            },
            container->underlying);
      }
      // I've believed that overloading the address-of operator was useless, but
      // ho! Here is a use!
      Iterator<false> operator&() { return Iterator<false>{container, index}; }
      Iterator<true> operator&() const {
        return Iterator<true>{container, index};
      }

    private:
      Reference(Self* container, size_t index) :
          container(container), index(index) {}

      Self* container;
      size_t index;

      template<size_t... i>
      auto convert_impl(std::index_sequence<i...>) const& {
        return std::make_tuple(std::get<i>(container->underlying)[index]...);
      }
      template<size_t... i> auto convert_impl(std::index_sequence<i...>) & {
        return std::make_tuple(std::reference_wrapper{
            std::get<i>(container->underlying)[index]}...);
      }
      template<size_t... i> auto convert_impl(std::index_sequence<i...>) && {
        return std::make_tuple(
            std::move(std::get<i>(container->underlying)[index])...);
      }

      friend Self;
    };

    /**
     * \brief An immutable 'reference' to an element of the container.
     *
     * Because of how elements are laid out in this container, this is not a
     * real reference, but rather a proxy type.
     *
     * Note that `std::get` does not work on this type. The `.get` method can be
     * used instead.
     */
    class ConstReference {
    public:
      operator value_type() const& {
        return convert_impl(std::index_sequence_for<Args...>{});
      }
      template<size_t i> const NthType<i, Args...>& get() const noexcept {
        return std::get<i>(container->underlying)[index];
      }
      template<typename T> const T& get() const noexcept {
        return std::get<find_type<T, Args...>>(container->underlying)[index];
      }
      Iterator<true> operator&() const {
        return Iterator<true>{container, index};
      }

    private:
      ConstReference(const Self* container, size_t index) :
          container(container), index(index) {}

      const Self* container;
      size_t index;

      template<size_t... i>
      auto convert_impl(std::index_sequence<i...>) const& {
        return std::make_tuple(std::get<i>(container->underlying)[index]...);
      }

      friend Self;
    };

    /**
     * \brief Return a `reference` to an element by index.
     *
     * \pre `0 <= i && i < size()`
     */
    Reference operator[](size_t i) noexcept { return Reference{this, i}; }
    /**
     * \brief Return a `const_reference` to an element by index.
     *
     * \pre `0 <= i && i < size()`
     */
    ConstReference operator[](size_t i) const noexcept {
      return ConstReference{this, i};
    }
    /**
     * \brief Return a `reference` to an element by index.
     *
     * Throws if `i` is out of range.
     */
    Reference at(size_t i) {
      if (i >= size()) throw std::out_of_range{"Index out of range"};
      return reference{this, i};
    }
    /**
     * \brief Return a `const_reference` to an element by index.
     *
     * Throws if `i` is out of range.
     */
    ConstReference at(size_t i) const {
      if (i >= size()) throw std::out_of_range{"Index out of range"};
      return ConstReference{this, i};
    }
    /**
     * \brief Return a `reference` to the first element of this collection.
     *
     * \pre `!empty()`
     */
    Reference front() noexcept { return (*this)[0]; }
    /**
     * \brief Return a `const_reference` to the first element of this
     * collection.
     *
     * \pre `!empty()`
     */
    ConstReference front() const noexcept { return (*this)[0]; }
    /**
     * \brief Return a `reference` to the last element of this collection.
     *
     * \pre `!empty()`
     */
    Reference back() noexcept { return (*this)[size() - 1]; }
    /**
     * \brief Return a `const_reference` to the last element of this collection.
     *
     * \pre `!empty()`
     */
    ConstReference back() const noexcept { return (*this)[size() - 1]; }

    /**
     * \brief Add an element to the back of the collection.
     *
     * \post `!empty()`
     */
    void push_back(const value_type& value) {
      tuple_for_each_index(
          [&value, this](auto& elem, auto i) {
            elem.push_back(std::get<i.value>(value));
          },
          underlying);
    }
    /**
     * \brief Add an element to the back of the collection.
     *
     * \post `!empty()`
     */
    void push_back(value_type&& value) {
      tuple_for_each_index(
          [&value](auto& elem, auto i) {
            elem.push_back(std::move(std::get<i.value>(value)));
          },
          underlying);
    }
    /**
     * \brief Set the `i`th element of the collection by constructing each
     * component of a tuple.
     *
     * \pre `0 <= i && i < size()`
     */
    template<typename... A> void set_element_piecewise(size_t i, A&&... args) {
      auto value = std::tuple(std::forward<A>(args)...);
      tuple_for_each_index(
          [i, &value](auto& elem, auto j) {
            std::apply(
                [i, &elem](auto&&... args) {
                  // Yeah, this monstrosity again.
                  using E = typename std::decay_t<decltype(elem)>::value_type;
                  elem[i] = E(std::forward<decltype(args)>(args)...);
                },
                std::get<j.value>(value));
          },
          underlying);
    }
    /**
     * \brief Add an element to the back of the collection by calling a
     * constructor.
     *
     * \post `!empty()`
     */
    template<typename... A> void emplace_back(A&&... args) {
      push_back(std::tuple(std::forward<A>(args)...));
    }
    /**
     * \brief Add an element to the back of the collection by calling a
     * constructor for each component of a tuple.
     *
     * \post `!empty()`
     */
    template<typename... A> void emplace_back_piecewise(A&&... args) {
      auto value = std::tuple(std::forward<A>(args)...);
      tuple_for_each_index(
          [&value](auto& elem, auto i) {
            std::apply(
                [&elem](auto&&... args) {
                  elem.emplace_back(std::forward<decltype(args)>(args)...);
                },
                std::get<i.value>(value));
          },
          underlying);
    }

    /**
     * \brief Remove the last element of this collection.
     *
     * \pre `!empty()`
     */
    void pop_back() noexcept {
      tuple_for_each([](auto& elem) { elem.pop_back(); }, underlying);
    }

    // Iteration
    /**
     * \brief An iterator type.
     *
     * \tparam is_const True if this iterator points to const data.
     */
    template<bool is_const> class Iterator {
      using Ref = std::conditional_t<is_const, ConstReference, Reference>;
      using CRef = ConstReference;

    public:
      using difference_type = typename Self::difference_type;
      using value_type = typename Self::value_type;
      using pointer = Iterator<is_const>;
      using reference = Ref;
      using iterator_category = std::random_access_iterator_tag;

      Iterator(const Iterator& other) = default;
      Iterator& operator=(const Iterator& other) = default;
      Iterator& operator++() {
        assert(index < container->size());
        ++index;
        return *this;
      }
      Iterator operator++(int) {
        Iterator old = *this;
        operator++();
        return old;
      }
      Ref operator*() {
        assert(index < container->size());
        return Ref{container, index};
      }
      CRef operator*() const {
        assert(index < container->size());
        return Ref{container, index};
      }
      Iterator& operator--() {
        assert(index > 0);
        --index;
        return *this;
      }
      Iterator operator--(int) {
        Iterator old = *this;
        operator--();
        return old;
      }
      Iterator& operator+=(ptrdiff_t n) {
        index += n;
        return *this;
      }
      Iterator operator+(ptrdiff_t n) {
        Iterator it2 = *this;
        it2 += n;
        return it2;
      }
      Iterator& operator-=(ptrdiff_t n) {
        index -= n;
        return *this;
      }
      Iterator operator-(ptrdiff_t n) {
        Iterator it2 = *this;
        it2 -= n;
        return it2;
      }
      Iterator operator-(Iterator other) {
        assert(container == other.container);
        return index == other.index;
      }
      Ref operator[](ptrdiff_t n) { return *(*this + n); }
      CRef operator[](ptrdiff_t n) const { return *(*this + n); }
      bool operator==(Iterator other) const {
        assert(container == other.container);
        return index == other.index;
      }
      bool operator!=(Iterator other) const {
        assert(container == other.container);
        return index != other.index;
      }
      bool operator<(Iterator other) const {
        assert(container == other.container);
        return index < other.index;
      }
      bool operator<=(Iterator other) const {
        assert(container == other.container);
        return index <= other.index;
      }
      bool operator>(Iterator other) const {
        assert(container == other.container);
        return index > other.index;
      }
      bool operator>=(Iterator other) const {
        assert(container == other.container);
        return index >= other.index;
      }

    private:
      using ContainerPtr = std::conditional_t<is_const, const Self*, Self*>;

      Iterator(ContainerPtr container, size_t index) :
          container(container), index(index) {}

      const ContainerPtr container;
      size_t index;

      friend Self;
    };
    using reference = Reference;
    using const_reference = ConstReference;
    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using pointer = iterator;
    using const_pointer = const_iterator;

    iterator begin() { return iterator{this, 0}; }
    iterator end() { return iterator{this, size()}; }
    const_iterator begin() const { return const_iterator{this, 0}; }
    const_iterator end() const { return const_iterator{this, size()}; }
    const_iterator cbegin() const { return const_iterator{this, 0}; }
    const_iterator cend() const { return const_iterator{this, size()}; }
    reverse_iterator rbegin() { return std::reverse_iterator{end()}; }
    reverse_iterator rend() { return std::reverse_iterator{begin()}; }
    const_reverse_iterator rbegin() const {
      return std::reverse_iterator{end()};
    }
    const_reverse_iterator rend() const {
      return std::reverse_iterator{begin()};
    }
    const_reverse_iterator crbegin() { return std::reverse_iterator{cend()}; }
    const_reverse_iterator crend() { return std::reverse_iterator{cbegin()}; }

  private:
    MapParameterPack<V, Args...> underlying;
  };

  template<typename T>
  constexpr bool is_tuple_vector = instantiates<TupleVector, T>;
}

#endif // SKL_TUPLEVECTOR_H
