#pragma once
#ifndef SKL_TEST_TYPES
#  define SKL_TEST_TYPES

/**
 * \brief Types useful for testing template code.
 */

namespace skl {
  /// Wraps a type and tracks how many times it is copied.
  template<typename T> class TrackCopy {
  public:
    template<typename... Args>
    TrackCopy(Args&&... args) : t(args...), copies(0) {}
    ~TrackCopy() = default;
    TrackCopy(const TrackCopy& other) : t(other.t), copies(copies + 1) {}
    TrackCopy(TrackCopy&& other) = default;
    TrackCopy& operator=(const TrackCopy& other) {
      t = other.t;
      copies = other.copies;
      return *this;
    }
    TrackCopy& operator=(TrackCopy&& other) = default;
    const T& item() const { return t; }
    size_t copy_count() const { return copies; }

  private:
    T t;
    size_t copies;
  };

  /// Wraps around T and prevents it from being moved.
  template<typename T> class Stationary {
  public:
    template<typename... Args> Stationary(Args&&... args) : t(args...) {}
    ~Stationary() = default;
    Stationary(const Stationary& other) = delete;
    Stationary(Stationary&& other) = delete;
    Stationary& operator=(const Stationary& other) = delete;
    Stationary& operator=(Stationary&& other) = delete;
    const T& item() const { return t; }

  private:
    T t;
  };

  /// Wraps around T and prevents it from being copied.
  template<typename T> class Unique {
  public:
    template<typename... Args> Unique(Args&&... args) : t(args...) {}
    ~Unique() = default;
    Unique(const Unique& other) = delete;
    Unique(Unique&& other) = default;
    Unique& operator=(const Unique& other) = delete;
    Unique& operator=(Unique&& other) = default;
    const T& item() const { return t; }

  private:
    T t;
  };
}

#endif // SKL_TEST_TYPES
