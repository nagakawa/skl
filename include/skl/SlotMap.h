#pragma once
#ifndef SKL_SLOT_MAP
#  define SKL_SLOT_MAP

#  include "skl/GenerationalAllocator.h"
#  include "skl/TupleVector.h"

namespace skl {
  /**
   * \brief A data structure wherein inserting values returns unspecified keys
   * that can later be looked up in constant time.
   *
   * This data structure supports the following operations:
   *
   * - Inserting a value, returning an iterator (from which the key can be
   *   retrieved): \f$O(1)\f$ amortised and \f$O(M)\f$ worst case
   * - Looking up a value by key: \f$O(1)\f$
   * - Removing a value by key: \f$O(1)\f$ amortised
   * - Iterating through all of the values: \f$O(M)\f$
   */
  template<typename Vec, typename Gen = uint64_t, typename Index = size_t>
  class SlotMap {
    // Has to be here; otherwise, the typedef for key_type doesn't know about
    // the allocator field.
  private:
    Vec underlying;
    GenerationalAllocator<Gen, Index> allocator;

  public:
    using Self = SlotMap<Vec, Gen, Index>;
    using key_type = typename decltype(allocator)::Handle;
    using mapped_type = typename Vec::value_type;
    using reference = typename Vec::reference;
    using const_reference = typename Vec::const_reference;
    using pointer = typename Vec::pointer;
    using const_pointer = typename Vec::const_pointer;
    using size_type = size_t;
    using difference_type = ptrdiff_t;
    constexpr static bool is_slot_map = true;

    SlotMap() = default;
    SlotMap(const SlotMap& other) = default;
    SlotMap(SlotMap&& other) = default;
    SlotMap& operator=(const SlotMap& other) = default;
    SlotMap& operator=(SlotMap&& other) = default;

    size_t size() const { return allocator.count_extant(); }

    // TODO(kozet): interface needs more work
    // A SlotMap is neither a map nor a set, so which one's interface should it
    // emulate?
    template<bool is_const> class Iterator {
      using Ref = std::conditional_t<
          is_const, typename Vec::const_reference, typename Vec::reference>;
      using CRef = typename Vec::const_reference;

    public:
      using difference_type = typename Self::difference_type;
      using value_type = typename Self::mapped_type;
      using pointer = std::decay_t<Ref>*;
      using reference = Ref;
      using iterator_category = std::bidirectional_iterator_tag;

      Iterator(const Iterator& other) = default;
      Iterator& operator=(const Iterator& other) = default;
      key_type key() const { return h; }
      Ref operator*() {
        assert(container->allocator.is_live(h));
        return container->underlying[h.index];
      }
      CRef operator*() const {
        assert(container->allocator.is_live(h));
        return container->underlying[h.index];
      }
      auto operator-> () { return &(operator*()); }
      auto operator-> () const { return &(operator*()); }
      bool operator==(Iterator other) const {
        assert(container == other.container);
        return h == other.h;
      }
      bool operator!=(Iterator other) const {
        assert(container == other.container);
        return h != other.h;
      }
      Iterator& operator++() {
        h = container->allocator.handle_after(h);
        return *this;
      }
      Iterator operator++(int) {
        Iterator old = *this;
        operator++();
        return old;
      }
      Iterator& operator--() {
        h = container->allocator.handle_before(h);
        return *this;
      }
      Iterator operator--(int) {
        Iterator old = *this;
        operator--();
        return old;
      }

    private:
      using ContainerPtr = std::conditional_t<is_const, const Self*, Self*>;

      Iterator(ContainerPtr container, key_type h) :
          container(container), h(h) {}

      const ContainerPtr container;
      key_type h;

      friend Self;
    };

    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    iterator begin() noexcept {
      return iterator{this, allocator.first_occupied()};
    }
    const_iterator begin() const noexcept {
      return const_iterator{this, allocator.first_occupied()};
    }
    const_iterator cbegin() const noexcept {
      return const_iterator{this, allocator.first_occupied()};
    }
    iterator end() noexcept {
      return iterator{this, key_type{0, allocator.num_slots()}};
    }
    const_iterator end() const noexcept {
      return const_iterator{this, key_type{0, allocator.num_slots()}};
    }
    const_iterator cend() const noexcept {
      return const_iterator{this, key_type{0, allocator.num_slots()}};
    }
    reverse_iterator rbegin() { return std::reverse_iterator{end()}; }
    reverse_iterator rend() { return std::reverse_iterator{begin()}; }
    const_reverse_iterator rbegin() const {
      return std::reverse_iterator{end()};
    }
    const_reverse_iterator rend() const {
      return std::reverse_iterator{begin()};
    }
    const_reverse_iterator crbegin() { return std::reverse_iterator{cend()}; }
    const_reverse_iterator crend() { return std::reverse_iterator{cbegin()}; }

    // TODO(kozet): should the insert methods return iterator or key_type?

    iterator insert(const mapped_type& value) {
      bool appended;
      key_type h = allocator.allocate(appended);
      if (appended)
        underlying.push_back(value);
      else
        underlying[h.index] = value;
      return iterator{this, h};
    }

    iterator insert(mapped_type&& value) {
      bool appended;
      key_type h = allocator.allocate(appended);
      if (appended)
        underlying.push_back(std::move(value));
      else
        underlying[h.index] = std::move(value);
      return iterator{this, h};
    }

    template<typename... Args> iterator emplace(Args&&... args) {
      bool appended;
      key_type h = allocator.allocate(appended);
      if (appended)
        underlying.emplace_back(std::forward<Args>(args)...);
      else
        underlying[h.index] = mapped_type{std::forward<Args>(args)...};
      return iterator{this, h};
    }

    template<typename... Args> iterator emplace_piecewise(Args&&... args) {
      static_assert(
          is_tuple_vector<Vec>,
          "This doesn't make sense unless the underlying storage is a "
          "TupleVector!");
      bool appended;
      key_type h = allocator.allocate(appended);
      if (appended)
        underlying.emplace_back_piecewise(std::forward<Args>(args)...);
      else
        underlying.set_element_piecewise(h.index, std::forward<Args>(args)...);
      return iterator{this, h};
    }

    iterator erase(key_type key) {
      assert(allocator.is_live(key));
      allocator.release(key);
      return ++iterator{this, key};
    }

    iterator erase(iterator pos) {
      assert(allocator.is_live(pos.key()));
      allocator.release(pos.key());
      return ++pos;
    }

    reference operator[](key_type key) {
      assert(allocator.is_live(key));
      return underlying[key.index];
    }

    const_reference operator[](key_type key) const {
      assert(allocator.is_live(key));
      return underlying[key.index];
    }

    reference at(key_type key) {
      if (!allocator.is_live(key))
        throw std::out_of_range{"Index out of range"};
      return underlying[key.index];
    }

    const_reference at(key_type key) const {
      if (!allocator.is_live(key))
        throw std::out_of_range{"Index out of range"};
      return underlying[key.index];
    }

    iterator find(key_type key) {
      if (!allocator.is_live(key)) { return end(); }
      return iterator{this, key};
    }

    const_iterator find(key_type key) const {
      if (!allocator.is_live(key)) { return end(); }
      return const_iterator{this, key};
    }
  };
}

#endif // SKL_SLOT_MAP
