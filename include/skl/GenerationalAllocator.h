#pragma once
#ifndef SKL_GENERATIONAL_ALLOCATOR_H
#  define SKL_GENERATIONAL_ALLOCATOR_H

#  include <assert.h>
#  include <stdint.h>

#  include <algorithm>
#  include <limits>
#  include <numeric>
#  include <vector>

namespace skl {
  /**
   * \brief A class that allocates (generation, index) pairs.
   *
   * This class allocates and frees slots, such that
   *
   * - once a (generation, index) pair is returned from the `allocate` method,
   *   it will never be returned from it again (or at least not in a long time)
   *   unless the allocator is cleared
   * - no two handles allocated at the same time from the same allocator
   *   has the same index
   *
   * These rules allow the index fields of handles to be used as indices into
   * arrays.
   *
   * This class was originally taken from TDR. It has been modified to lie in
   * the `skl` namespace and use `snake_case` identifiers where it had been
   * using `camelCase` ones.
   */
  template<typename Gen = uint64_t, typename Index = size_t>
  class GenerationalAllocator {
  public:
    struct Handle {
      Gen generation;
      Index index;
      bool operator==(Handle other) const {
        return generation == other.generation && index == other.index;
      }
      bool operator!=(Handle other) const { return !(*this == other); }
    };

    GenerationalAllocator() = default;
    GenerationalAllocator(const GenerationalAllocator& other) = default;
    GenerationalAllocator(GenerationalAllocator&& other) = default;
    GenerationalAllocator&
    operator=(const GenerationalAllocator& other) = default;
    GenerationalAllocator& operator=(GenerationalAllocator&& other) = default;

    /**
     * \brief Return whether the handle is valid; i.e. it is within bounds of
     * the generation array and its generation is current.
     *
     * This operation has \f$O(1)\f$ time complexity.
     */
    bool is_valid(Handle h) const noexcept {
      return h.index < generations.size() &&
             generations[h.index] == h.generation;
    }
    /**
     * \brief Return whether the handle is live – in other words, it was
     * allocated but not freed.
     *
     * This operation has \f$O(1)\f$ time complexity.
     */
    bool is_live(Handle h) const noexcept {
      return is_valid(h) && !free_bits[h.index];
    }
    /**
     * Return whether there is a live handle at a certain index.
     *
     * This operation has \f$O(1)\f$ time complexity.
     */
    bool is_live(Index i) const noexcept { return !free_bits[i]; }
    /**
     * Return the generation associated with the index.
     *
     * This operation has \f$O(1)\f$ time complexity.
     */
    Gen get_generation(Index i) const noexcept { return generations[i]; }
    /**
     * Allocate a handle.
     *
     * This operation has \f$O(1)\f$ amortised time complexity, and \f$O(M)\f$
     * in the worst case, where \f$M\f$ is the maximum number of handles that
     * have been live at the same time since the last `clear()`.
     */
    /// Allocate a handle.
    Handle allocate(bool& appended) {
      if (free_list.empty()) {
        Index i = (Index) generations.size();
        assert(i != std::numeric_limits<Index>::max());
        generations.push_back(0);
        free_bits.push_back(false);
        appended = true;
        return Handle{0, i};
      }
      Index i = free_list.back();
      free_list.pop_back();
      Gen g = ++generations[i];
      free_bits[i] = false;
      appended = false;
      return Handle{g, i};
    }
    /**
     * \brief Allocates multiple handles from this allocator more efficiently
     * than calling `allocate` multiple times. I hope.
     *
     * After calling this function, the pointer `handles` will point to a block
     * of `count` elements, so that elements [0, existing) will be indices
     * that were not newly appended in the allocator (instead being recycled
     * from already-freed elements) and [existing, count) will consist of a
     * sequence of appended handles whose indices ascend consecutively.
     *
     * This operation has \f$O(k)\f$ amortised time complexity, and \f$O(M)\f$
     * in the worst case, where \f$k\f$ is the number of handles to generate and
     * \f$M\f$ is the maximum number of handles that have been live at the same
     * time since the last `clear()`.
     *
     * \param count The number of handles to allocate.
     * \param handles A block to `count` `Handle`s to store the new handles at.
     * \param existing The number of handles that are **not** appended.
     */
    void allocm(Gen count, Handle* handles, size_t& existing) {
      size_t n_free = std::min(count, free_list.size());
      for (size_t i = 0; i < n_free; ++i) {
        auto e = free_list[i];
        handles[i] = Handle{++generations[e], e};
        free_bits[e] = false;
      }
      free_list.erase(free_list.begin(), free_list.begin() + n_free);
      size_t n_to_append = count - n_free;
      Index i = (Index) generations.size();
      generations.insert(generations.end(), n_to_append, 0);
      free_bits.insert(free_bits.end(), n_to_append, false);
      std::generate(handles + n_free, handles + count, [&i]() {
        return Handle{0, i++};
      });
      existing = n_free;
    }
    /**
     * \brief Free a handle.
     *
     * The handle must be live.
     *
     * This operation has \f$O(1)\f$ amortised time complexity, and
     * \f$O(M - N)\f$ in the worst case, where \f$M\f$ is the maximum number of
     * handles that have been live at the same time since the last `clear()`,
     * and \f$N\f$ is the number of handles that are currently live.
     */
    void release(Handle h) {
      assert(is_live(h));
      free_bits[h.index] = true;
      free_list.push_back(h.index);
    }
    /**
     * \brief Free a handle associated with an index.
     *
     * The handle must be live.
     *
     * This operation has \f$O(1)\f$ time complexity.
     */
    void release(size_t i) {
      assert(is_live(i));
      free_bits[i] = true;
      free_list.push_back(i);
    }
    /**
     * \brief Clear the allocator.
     *
     * This invalidates all existing handles.
     *
     * This operation has \fO(M)\f time complexity, where \f$M\f$ is the maximum
     * number of handles that have been live at the same time since the last
     * `clear()`.
     */
    void clear() {
      std::fill(free_bits.begin(), free_bits.end(), true);
      free_list.resize(generations.size());
      std::iota(free_list.begin(), free_list.end(), 0);
    }
    /**
     * \brief Count the number of live handles.
     *
     * This operation has \fO(M)\f time complexity, where \f$M\f$ is the maximum
     * number of handles that have been live at the same time since the last
     * `clear()`.
     */
    size_t count_extant() const noexcept {
      return std::count_if(
          free_bits.begin(), free_bits.end(), [](bool f) { return !f; });
    }
    /**
     * \brief Count the number of 'slots' in the allocator – in other words, the
     * maximum number of handles that have been live at the same time since the
     * last `clear()`.
     */
    size_t num_slots() const noexcept { return free_bits.size(); }

    Handle first_occupied() const noexcept {
      for (Index i = 0; i < free_bits.size(); ++i) {
        if (!free_bits[i]) { return Handle{generations[i], i}; }
      }
      return Handle{0, free_bits.size()};
    }

    Handle handle_after(Handle h) const noexcept {
      assert(h.index >= 0 && h.index < free_bits.size());
      for (Index i = h.index + 1; i < free_bits.size(); ++i) {
        if (!free_bits[i]) { return Handle{generations[i], i}; }
      }
      return Handle{0, free_bits.size()};
    }

    Handle handle_before(Handle h) const noexcept {
      assert(h.index <= free_bits.size());
      for (Index i = h.index - 1; i != (Index) -1; --i) {
        if (!free_bits[i]) { return Handle{generations[i], i}; }
      }
      assert(false && "Handle is first_occupied!");
      return Handle{0, 0};
    }

  private:
    std::vector<Index> free_list;
    std::vector<Gen> generations;
    std::vector<bool> free_bits;
  };
}

#endif // SKL_GENERATIONAL_ALLOCATOR_H
